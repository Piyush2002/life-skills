### Question 1: Steps/Strategies for Active Listening

1. **Give Your Full Attention:** When engaging in a conversation, focus entirely on the speaker, minimizing distractions and maintaining eye contact.

2. **Demonstrate Engagement:** Utilize non-verbal cues such as nodding and smiling to convey attentiveness and encourage the speaker.

3. **Confirm Understanding:** Paraphrase the speaker's words to ensure comprehension and address any potential misunderstandings.

4. **Encourage Elaboration:** Pose open-ended questions that prompt the speaker to provide detailed responses, fostering a deeper conversation.

5. **Respectful Silence:** Avoid interrupting the speaker, allowing them to express their thoughts fully before responding.

6. **Express Empathy:** Validate the speaker's emotions and experiences to establish a meaningful connection.

### Question 2: Key Points of Reflective Listening (Fisher's Model)

Fisher's model of reflective listening underscores the importance of:

- **Mirroring Body Language:** Reflecting the speaker's non-verbal cues such as gestures and posture.

- **Acknowledging Emotions:** Validating and recognizing the emotions conveyed by the speaker.

- **Verifying the Message:** Confirming understanding by seeking clarification or summarizing the speaker's message.

### Question 3: Obstacles in Your Listening Process

Obstacles in effective listening may include:

- **Distractions:** External or internal factors diverting attention from the speaker.

- **Prejudice:** Pre-existing biases influencing objective listening.

- **Lack of Interest:** Personal disinterest in the topic or speaker affecting attention.

- **Multitasking:** Attempting to do too many things simultaneously, compromising comprehension.

- **Assumptions:** Presuming knowledge of the speaker's message, leading to misinterpretation.

### Question 4: Improving Your Listening

To enhance listening skills:

- **Minimize Distractions:** Choose a quiet setting for important conversations.

- **Remain Open-Minded:** Avoid preconceived judgments and biases.

- **Demonstrate Interest:** Express genuine interest in the speaker and their message.

- **Provide Feedback:** Offer feedback and seek clarification to showcase engagement.

- **Practice Mindfulness:** Concentrate on the present moment during conversations.

### Question 5: Switching to Passive Communication Style

A passive communication style may be adopted to evade conflict or when expressing opinions seems unproductive, often stemming from a lack of assertiveness.

### Question 6: Switching to Aggressive Communication Style

An aggressive communication style may emerge when feeling threatened or frustrated, reflecting a struggle to manage emotions effectively.

### Question 7: Switching to Passive-Aggressive Communication Style

Passive-aggressive communication may surface when dissatisfaction is expressed indirectly, often through sarcasm or subtle taunts rather than addressing the issue directly.

### Question 8: Making Communication Assertive

To communicate assertively:

- **Express Feelings and Needs:** Clearly articulate feelings and needs without aggression.

- **Use "I" Statements:** Frame thoughts using "I" to avoid blaming others.

- **Respect Others:** Acknowledge others' opinions and emotions while asserting one's own.

- **Practice Active Listening:** Pay close attention to others, validating their concerns.

- **Set Boundaries:** Establish clear boundaries to protect personal rights without infringing on others'.