# Energy Management

## 1. Manage Energy not Time

### Question 1:

What are the activities you do that make you relax - Calm quadrant?

I find relaxation in activities such as reading a book, practicing meditation, or taking a leisurely walk in nature.

### Question 2:

When do you find getting into the Stress quadrant?

I often find myself in the Stress quadrant when facing tight deadlines at work or dealing with unexpected challenges for example not able to solve the coding questions during contests.

### Question 3:

How do you understand if you are in the Excitement quadrant?

I know I'm in the Excitement quadrant when I feel a surge of positive energy and enthusiasm, often accompanied by a sense of anticipation and motivation.


## 4. Sleep is your superpower
### Question 4

Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

In the video, the importance of sleep is highlighted as a superpower for overall well-being. Key points include the impact of sleep on cognitive function, emotional resilience, and physical health.

### Question 5

What are some ideas that you can implement to sleep better?

To improve sleep, one can establish a consistent sleep routine, create a comfortable sleep environment, limit screen time before bed, practice relaxation techniques like meditation, and ensure physical activity during the day.

## 5. Brain Changing Benefits of Exercise

### Question 6

Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

1. Exercise enhances brain function.
2. It promotes the growth of new neurons.
3. Improves memory and cognitive abilities.
4. Reduces the risk of neurodegenerative diseases.
5. Boosts mood and reduces stress through the release of endorphins.

### Question 7

What are some steps you can take to exercise more?  

Incorporating more physical activity can be achieved by scheduling regular workouts, engaging in enjoyable physical activities, joining fitness classes, setting realistic fitness goals, and incorporating movement into daily routines, such as taking the stairs or walking instead of driving.
