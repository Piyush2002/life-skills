# Tiny Habits

## 1. Tiny Habits - BJ Fogg

### Question 1

In this video, what was the most interesting story or idea for you?

According to the video, the most interesting story or idea is about using tiny habits to make lasting changes in one's life. The speaker of the video, BJ Fogg, is a behavior scientist who studies how to change people's behavior.

Here are the key points of the talk:

* Tiny habits are small, easy behaviors that you can easily add to your existing routines. For example, flossing one tooth after you brush your teeth, or doing two push-ups after you use the restroom.
* Tiny habits are effective because they are easy to do and they trigger the release of dopamine, a neurotransmitter that helps to reinforce the behavior.
* Tiny habits can be used to create long-term change. By starting with small, easy behaviors, you can gradually build up to more challenging behaviors over time.

The speaker also shares a personal story about how he used tiny habits to lose weight. He started by simply stepping on the scale every day, and then gradually added other tiny habits, such as drinking a glass of water before each meal. Over time, these small changes added up to a significant weight loss.



## 2. Tiny Habits by BJ Fogg - Core Message

### Question 2

How can you use B = MAP to make making new habits easier? What are M, A and P.

According to the video, B = MAP stands for Behavior equals Motivation Ability Prompt. This formula is designed to help you understand what you need to have in place in order to consistently perform a desired behavior.

**Motivation :** This refers to your desire or willingness to perform the behavior.
**Ability :** This refers to your physical and mental capacity to perform the behavior.
**Prompt :** This refers to the cue or trigger that reminds you to perform the behavior.


### Question 3

Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

The key takeaway from this video is that you should shrink down your desired behavior to the tiniest version possible and pair it with an action prompt to increase your chances of success.

* Celebrating even small wins helps you build confidence and momentum. When you feel successful, even if it's just for completing a tiny habit, you're more likely to want to do it again. This positive reinforcement helps you to stick with the habit over time.
* Celebrating small wins is more effective than waiting to celebrate big successes. This is because small wins are more frequent, which helps you to stay motivated in the long run.


## 3. 1% Better Every Day Video

### Question 4

**In this video, what was the most interesting story or idea for you?**

According to the video, the most interesting story or idea is the story of Dave Brailsford and his philosophy of marginal gains.

Brailsford took over British Cycling in 2000, at a time when the team was performing poorly. He introduced a philosophy of marginal gains, which involved making small improvements in every area, believing that these small improvements would add up to significant results. The team implemented a number of changes, such as using lighter tires, having riders wear more aerodynamic clothing, and getting enough sleep. As a result of these changes, British Cycling went on to win four out of the next five Tour de France titles. This story is interesting because it shows how small changes can lead to big results. It also highlights the importance of consistency and focus on the details. 

## 4. Book Summary of Atomic Habits

#### Question 5
What is the book's perspective about Identity?

* **Identity is the north star of habit change:** We should focus on changing our identity to change our habits.
* **Work from identity to outcomes:** Instead of saying "I want to get rid of belly fat," say "I am a healthy person."
* **Habits are formed through a four-step loop:** Cue, craving, response, reward.
* **Four laws of behavior change:**
    * Make it obvious: Design your environment for desired cues.
    * Make it attractive: Make the habit appealing.
    * Make it easy: Reduce friction and prime your environment.
    * Make it immediately satisfying: Attach immediate gratification.


#### Question 6
Write about the book's perspective on how to make a habit easier to do?

* **Focus on small changes and building systems:** Small improvements compound over time.
* **Focus on systems instead of goals:** Systems prioritize progress, not just achieving goals.
* **Identity change is key:** View yourself as someone embodying the desired habit.
* **The four laws of behavior change (applied to make habits easier):**
    * Make desired cues obvious in your environment.
    * Associate desired habits with positive rewards.
    * Reduce friction to perform desired habits.
    * Associate desired habits with immediate rewards.

#### Question 7
Write about the book's perspective on how to make a habit harder to do?

* **Make the cue invisible:** Remove triggers for undesired habits.
* **Make the action unattractive, difficult:** Increase friction for undesired habits.
* **Make the reward unsatisfying:** Remove positive reinforcement for undesired habits.
* **Change your environment:** Make desired cues obvious and undesired cues invisible.
* **Reduce friction for desired habits:** Make them easier to perform.


## 5. Reflection:

#### Question 8:
Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

**Building the Gym Habit:**

**Strategies:**

* **Make it obvious:**
    * Place gym bag and clothes by your bed for a visual cue.
    * Find a gym buddy for accountability and social interaction.
* **Make it attractive:**
    * Listen to music or podcasts while exercising.
    * Track progress using a fitness app or journal to see improvements.
* **Make it easy:**
    * Pack your gym bag the night before.
    * Pre-plan your workouts to avoid wasting time at the gym.
* **Make it satisfying:**
    * Reward yourself after completing a workout (healthy treat, relaxation time, watching a favorite show).
    * Focus on the positive feelings after exercise (physical and mental well-being).
    * Celebrate milestones (reaching goals or consistent gym attendance).


#### Question 9:
Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

**Habit:** Excessive gaming.

**Strategies:**

* **Make the cue invisible:**
    * Turn off gaming app/platform notifications.
    * Remove gaming apps from your phone or hide them on your computer.
    * Unplug gaming consoles when not in use.
* **Make it unattractive:**
    * Reflect on the negative consequences of excessive gaming (sleep, productivity, social life).
    * Find alternative activities you enjoy (hobbies, spending time with loved ones, pursuing new interests).
* **Make it hard:**
    * Set time limits for gaming (use apps or website blockers).
    * Inform family or friends about your goal (gain their support and accountability).
    * Reward yourself for not gaming (allocate time saved to other enjoyable activities).
* **Make it unsatisfying:**
    * Play in a less comfortable environment (unrelaxing chair, playing with lights on).
    * Don't play your favorite games (opt for less engaging or enjoyable ones).
    * Reflect on the feeling after excessive gaming (tiredness, unproductivity, isolation).

