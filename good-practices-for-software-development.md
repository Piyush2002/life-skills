# Good Practices for Software Development

## Question 1
### Which point(s) were new to you?

- **Always over-communicate:** The idea of over-communication was highlighted, particularly in scenarios like informing about changes in requirements, unexpected issues during implementation, or technical difficulties like internet or device failures. This underscores the importance of keeping the team updated at all times, even if the updates seem minor. Additionally, the suggestion to keep video on during meetings for better rapport and communication was insightful, as it adds a personal touch to remote interactions and fosters stronger connections within the team.

- **Stuck? Ask questions:** The approach to asking questions when encountering obstacles in software development was detailed. It stressed the importance of framing questions effectively by providing clear explanations of the problem, mentioning attempted solutions, and utilizing tools like screenshots or code snippets for better context. Furthermore, the recommendation to observe how issues are reported in large open-source projects provides a valuable insight into effective communication within a development community.

## Question 2
### Which area do you think you need to improve on? What are your ideas to make progress in that area?

- **Improvement Area:** Upon reflection, I realize that I could improve in the aspect of over-communication. While I do strive to maintain regular communication with my team, there are instances where I may overlook informing them about minor changes or issues, assuming they are not significant. However, as emphasized in the text, even seemingly minor updates can contribute to better transparency and collaboration within the team.

- **Ideas for Progress:** To address this improvement area, I plan to adopt a more proactive approach to communication. This involves regularly updating my team about any developments, regardless of their perceived significance, to ensure everyone is on the same page. Additionally, I will prioritize using group communication channels over private messages to encourage transparency and accountability within the team. Furthermore, I will make a conscious effort to keep my video turned on during meetings to foster better connections with my colleagues, as suggested in the text. This not only enhances communication but also contributes to a more engaging and productive remote work environment.
