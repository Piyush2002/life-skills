
## 1. Grit
### Question 1    
Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

**Grit Video Summary:**

1. The video underscores the significance of grit in attaining long-term objectives.
   
2. It emphasizes the crucial role of perseverance and passion in the pursuit of success.

3. Viewers are encouraged to develop grit as a fundamental quality for achieving success in diverse aspects of life.

## 2. Introduction to Growth Mindset
### Question 2  : 

Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

The video introduces the concept of a growth mindset, stressing the belief that abilities and intelligence can be developed through dedication and effort. It encourages viewers to adopt a positive mindset, embrace challenges, and view failures as opportunities for learning and growth.


## 3. Understanding Internal Locus of Control

### Question 3
What is the Internal Locus of Control? What is the key point in the video?

**Internal Locus of Control:**

1. The Internal Locus of Control is the belief that individuals have control over their own actions and outcomes.

**Key Point in the Video - The Locus Rule:**

1. The video emphasizes the importance of cultivating an internal locus of control for staying motivated.

2. Taking responsibility for one's actions and believing in the ability to influence and shape one's own destiny is highlighted.

3. Cultivating an internal locus of control contributes to a more motivated and proactive approach to challenges.


## 4. How to build a Growth Mindset

### Question 4
What are the key points mentioned by speaker to build growth mindset (explanation not needed).


## How to build a Growth Mindset Video

**Key points mentioned by Brendon Burchard to build growth mindset:**

* **Believe in your ability to figure things out :**
   This is the most important thing. You need to believe that you can learn and improve, even when things are difficult.

* **Question your assumptions :**
  Don't limit yourself by thinking that you are not capable of something. Challenge your negative thoughts and beliefs.
* **Develop your own life curriculum :**
  Take charge of your own learning and growth. Don't wait for others to teach you what you need to know.

* **Honor the struggle:**
  When you face challenges, don't give up. See them as opportunities to learn and grow.


## 4. Mindset - A MountBlue Warrior Reference Manual

### Question 5

What are your ideas to take action and build Growth Mindset?

* **Embrace challenges:** View difficulties as learning opportunities.
* **Value effort:** Focus on progress and continuous learning.
* **Seek feedback:** Use it to improve and learn from others.
* **Stay positive:** Maintain a positive attitude and learn from failures.